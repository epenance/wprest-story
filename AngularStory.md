# Angular + WP-REST = AWESOME
Last year i made a small website for a client, almost free as it was my first chance to try out Angular 2. It was a great experience and i was super happy to actually get to learn something new while getting paid for it.

There was many minor speed bumps on the way, some bigger than others, especially getting the app to work with **Angular Universal** while using Angular 2 in its release candidate state was quite the trouble, but super pleasing once i overcame it.

## Reboot
Angular 4 is released, a lot of new toys are at the block, ofcourse we need to try them out!

For a long time I've wanted to do over the project and make it more dynamic so that I could use it as a building block for new clients. There is a ton of stuff in the old project I'll be doing different as I back then already knew it was a bad idea to do it the way i did it, but being new to Angular I had no idea how else to do it.

**My new stack:**

* Angular
* ngrx
* Angular Animations
* WordPress REST API
* Advanced Custom Fields (WP Plugin)
* JWT-Auth (WP Plugin)

**My goals for this project:**

I want to create a Universal SPA (Single page application) using WordPress and its REST API as the backend, and Angular in the frontend.

I want to be able to create, edit and update pages, menus, posts, from the frontend when I'm logged in as admin, to make it easier for clients to visualize how their website will look while creating the content on it.

I want the pages and posts to be build of different components that the clients can drag and drop in from a drawer. Theese should always looks exactly like the website will for people who are not logged in, with the exception of 2 icons, an edit and a settings icon.

When clicking on the edit icon, the text of the block should transform into a wysiwyg-editor where they can change the content of the block easily, and press submit to instantaniously see the updated version of the block.

When pressing the settings icon, you should have simple layout options such as text alignment, padding, margin, background colors etc.

##Where to start and how to overcome challenges
The very first thing i did when i started this project was making a quick and very rough prototype of the frontend editing, i wanted to test if it was possible for me to build dynamic pages but adding blocks in the frontend and changing the content of already existing blocks, and in the end save it in WordPress by sending a request to my API with a simple JSON object with my details in.

After a few hours i was up and running with a working prototype, i managed to implement the JWT Auth plugin while at it, but had the token hardcoded rather than grapping it from WordPress after a login. 

I used the Froala editor and made a few endpoints in the API myself so that i could upload new images to the WordPress media library and also use previous uploaded media.

It wasnt pretty, but it worked.

### State management
As soon as i wanted to implement some kind of real login it became obvious to me that i needed some better state management, I used Redux and MobX before when developing React applications and quickly got my eyes on **ngrx**.

I opened up the somewhat lacklusting documentation, looked over the examples and was completly overwhelmed, i decided to start over in a completly new project so that i could wrap my head around just ngrx with stores and effects rather than implementing it in the existing project.

I watched a quick 10 minutes tutorial on youtube, and decided to dive in. First i made a small user store that held the simple information ```loggedIn: true/false```, i added an action to login and one to log out and put up some buttons and a text component to show the state when it was changing the state by toggling the state.

I then remembered how much trouble it was to do async stuff in Redux in the early days where i was playing with it, needing Redux thunks and all kinds of workaround to make it work decent. I was pleasently suprised by how easy it was to setup simple ngrx/effects to listen to events and do my side effects such as logging in to wordpress by a http request and then updating the state afterwards.

### Fetching and showing data from WordPress when navigating

Fetching content from WordPress is fairly simple out of the box you simply send a GET request to ```/wp/v2/pages/<id>``` however the ID is just a number in the WordPress page table and it would not be all that attractive for our routes in our app to be called ```www.yourdomain.com/14/32``` and so on. Luckily its possible to search for pages by slug too, simply by sending a GET request to ```www.yourdomain.com/?slug=<slug>``` this will return an array rather than a single page so you need to select the first in this array.

Like I said earlier I wanted to ignore the default content field in WordPress and use Advanced Custom Fields instead to create dynamic content blocks. In my previous project i had a container that would then do a ```ngFor``` and loop over all the blocks, within the ngFor i would do a ```ngIf``` for each of my components saying something a like ```*ngIf="block.type === 'simple-text'"``` and so on. When i wrote that code last year, i already knew it was a terrible idea, but at the time it was the only way i knew how to make it work too, and hey it did but it wasnt pretty.

This time around however I had been at several meetups and was lucky enough to see a talk by a folloe Angular enthusiast who had been running his head against the same walls as i did last year, but he also found a solution.

I could make a simple directive that would take the block.type and match it up with a component within an array. It would then create the component with the elementRenderer from Angular and instanciate it for me. This made for a much cleaner code and a lot easier to maintain.

So now all the routes simply hit a single component which then looped over each of the blocks on the page and made the components matching the type of th block.

### Caching and animations
The next thing on my agenda was to find a solid way to cache pages that i had already loaded so that i did not need to hit the server without reason to do so. My initial idea was to save the pages in localstorage so that they could work cross tabs, but it quickly became clear that it would be a pain for the content editors to get rid of the cached pages that way, as it would not go away by a simple refresh.

For that reason i decided to simply make a small service which at the current time has 2 functions, ```getPage(slug)```and ```setPage(slug, data)```, when setPage is called it simply saves it by the key in a array of pages, and when ever i navigate to a route it will look for the page within the array before requesting it from WordPress if it doesnt exist. This way I could easily get new content from the server by simply refreshing the page.

The caching made the pages now instantly load if they was previously visited making the app insanely quick to navigate. This however introduced a new issue when i wanted to animate between the pages, as it was loading so fast there was no time to do my fadeout / fadein animation. a simple ```setTimeout(getPage(slug), 300)``` solved this however making 
time enough for the animation without making the app seem slow, but instead elegant.

### To come
My next moves will be reintegrating the edit/update functionality on the frontend as well as creating and styling the new block components. 

After that I need to create the drawer with the different kind of blocks, and make it possible to drag and drop them onto the page as well as reorder old blocks.

I also want to be able to offer the option to create and delete pages from the frontend, together with some sort of menu manager so that you pretty much never need to visit the WordPress backend again.
